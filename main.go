package main

import (
	"errors"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	. "http-server/api"
	"net/http"
	"os"
)

func main() {

	fmt.Println("DB connection open")

	http.HandleFunc("/", HandleRequest)

	err := http.ListenAndServe(":3000", nil)
	if errors.Is(err, http.ErrServerClosed) {
		fmt.Printf("server closed\n")
	} else if err != nil {
		fmt.Printf("error starting server: %s\n", err)
		os.Exit(1)
	}

}
