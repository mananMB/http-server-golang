package handler

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/joho/godotenv"
	"net/http"
	"net/url"
	"os"
	"strconv"
)

type Product struct {
	Id          int    `json:"id"`
	Title       string `json:"title"`
	Price       string `json:"price"`
	Category    string `json:"category"`
	Description string `json:"description"`
	Image       string `json:"image"`
}

func error(w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusInternalServerError)
}

func scanRows(rows *sql.Rows, w http.ResponseWriter) []Product {
	var products []Product

	for rows.Next() {
		var product Product

		err := rows.Scan(&product.Id, &product.Title, &product.Price, &product.Category, &product.Description, &product.Image)
		if err != nil {
			error(w)
			return nil
		}
		products = append(products, product)
	}

	return products
}

func getSingleProduct(db *sql.DB, id int, w http.ResponseWriter) []Product {
	var products []Product

	result, err := db.Query(fmt.Sprintf("SELECT * FROM products WHERE id=%d", id))
	if err != nil {
		error(w)
		return nil
	}
	products = scanRows(result, w)

	return products
}

func handleProductPatch(db *sql.DB, p url.Values, w http.ResponseWriter) {
	var products []Product
	var productBeingUpdated []Product
	productId, err := strconv.Atoi(p.Get("id"))
	productBeingUpdated = getSingleProduct(db, productId, w)
	if err != nil {
		error(w)
		return
	}
	title := p.Get("title")
	price := p.Get("price")
	category := p.Get("category")
	description := p.Get("description")
	image := p.Get("image")

	if title == "" {
		title = productBeingUpdated[0].Title
	}
	if price == "" {
		price = productBeingUpdated[0].Price
	}
	if category == "" {
		category = productBeingUpdated[0].Category
	}
	if description == "" {
		description = productBeingUpdated[0].Description
	}
	if image == "" {
		image = productBeingUpdated[0].Image
	}

	var newProductData = Product{
		Id:          productId,
		Title:       title,
		Price:       price,
		Category:    category,
		Description: description,
		Image:       image,
	}

	update, err := db.Prepare("UPDATE products SET title = ?, price = ?, category = ?, description = ?, image = ? WHERE id = ?")

	if err != nil {
		error(w)
		return
	}

	_, err = update.Exec(newProductData.Title, newProductData.Price, newProductData.Category, newProductData.Description, newProductData.Image, newProductData.Id)

	if err != nil {
		error(w)
		return
	}
	err = update.Close()
	if err != nil {
		error(w)
		return
	}

	products = getSingleProduct(db, productId, w)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	err = json.NewEncoder(w).Encode(products)
	if err != nil {
		error(w)
		return
	}
}
func handleProductsGet(db *sql.DB, p url.Values, w http.ResponseWriter) {
	var products []Product

	productId, err := strconv.Atoi(p.Get("id"))
	if err != nil {
		result, err := db.Query("SELECT * FROM products")
		if err != nil {
			error(w)
			return
		}
		products = scanRows(result, w)
	} else {
		products = getSingleProduct(db, productId, w)
	}

	if len(products) == 0 {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
	} else {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		err = json.NewEncoder(w).Encode(products)
		if err != nil {
			error(w)
			return
		}
	}
}

func handleProductsPost(db *sql.DB, postParams url.Values, w http.ResponseWriter) {
	var products []Product

	productId, err := strconv.Atoi(postParams.Get("id"))
	if err != nil {
		error(w)
		return
	}

	var product = Product{Id: productId, Title: postParams.Get("title"), Price: postParams.Get("price"), Description: postParams.Get("description"), Category: postParams.Get("category"), Image: postParams.Get("image")}

	query := `INSERT INTO products(id, title, price, category, description, image) VALUES (?, ?, ?, ?, ?, ?)`

	insert, err := db.Prepare(query)
	if err != nil {
		error(w)
		return
	}

	_, err = insert.Exec(product.Id, product.Title, product.Price, product.Category, product.Description, product.Image)
	if err != nil {
		error(w)
		return
	}
	err = insert.Close()
	if err != nil {
		error(w)
		return
	}

	products = getSingleProduct(db, productId, w)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	err = json.NewEncoder(w).Encode(products)
	if err != nil {
		error(w)
		return
	}
}

func handleProductsDelete(db *sql.DB, p url.Values, w http.ResponseWriter) {
	productId, err := strconv.Atoi(p.Get("id"))
	if err != nil {
		error(w)
		return
	}

	deleteProduct, err := db.Prepare("DELETE FROM products WHERE id=?")
	if err != nil {
		error(w)
		return
	}

	_, err = deleteProduct.Exec(productId)
	if err != nil {
		error(w)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusNoContent)
}

func HandleRequest(w http.ResponseWriter, r *http.Request) {
	err := godotenv.Load(".env")

	dbPassword := os.Getenv("DB_PASSWORD")
	dataSourceName := fmt.Sprintf("root:%s@tcp(127.0.0.1:3306)/http_golang_server", dbPassword)

	if err != nil {
		error(w)
		return
	}

	db, err := sql.Open("mysql", dataSourceName)

	if err != nil {
		error(w)
		return
	}

	defer func(db *sql.DB) {
		err := db.Close()
		if err != nil {
			error(w)
			return
		}
	}(db)

	switch r.Method {
	case "POST":
		handleProductsPost(db, r.URL.Query(), w)
	case "PATCH":
		handleProductPatch(db, r.URL.Query(), w)
	case "DELETE":
		handleProductsDelete(db, r.URL.Query(), w)
	default:
		handleProductsGet(db, r.URL.Query(), w)
	}
}
